#include <clang/Frontend/FrontendPluginRegistry.h> 
#include <clang/AST/ASTConsumer.h>
#include <clang/AST/Type.h>
#include <clang/AST/DeclCXX.h>
#include <clang/AST/RecursiveASTVisitor.h>
#include <clang/Frontend/CompilerInstance.h> 

using namespace clang;

namespace 
{


class PGVisitor : public RecursiveASTVisitor<PGVisitor> 
{
public:
    virtual bool VisitCXXRecordDecl(CXXRecordDecl *D)
    {
        if (D->isStruct())
            llvm::errs() << "struct ";
        else if (D->isClass())
            llvm::errs() << "class ";
        else
            return true;
        llvm::errs() << D->getNameAsString() << "\n";
        for(RecordDecl::field_iterator it = D->field_begin(), e = D->field_end(); it != e; ++it)
        {
            const FieldDecl* FD = *it;
            llvm::errs() << "  " << FD->getTypeSourceInfo()->getType().getAsString()  << "  " << FD->getNameAsString() << "\n";
        }


/*        for (RecordDecl::field_iterator it = D->field_begin(), e = D->field_end(); it != e; ++it)        {
            const FieldDecl* FD = *it;
            llvm::errs() << "  " << FD->getTypeSourceInfo()->getType().getAsString()  << "  " << FD->getNameAsString() << "\n";
        }*/
        return false; // do not recurse further
    }
};



class  CLPGConsumer : public ASTConsumer
{
public:
	PGVisitor* visitor;
    CLPGConsumer() : visitor(new PGVisitor()) { }

	virtual bool HandleTopLevelDecl(DeclGroupRef DG)
	{
		for (DeclGroupRef::iterator i = DG.begin(), e = DG.end(); i != e; ++i) 
		{
	        	visitor->TraverseDecl(*i);
		}
		return true;
	}
	
};

class CLPGASTAction : public PluginASTAction
{
protected:
	ASTConsumer* CreateASTConsumer(CompilerInstance& compiler, llvm::StringRef inFile)
	{
        llvm::errs() << "new CLPGConsumer()";
		return new CLPGConsumer();
	}

	bool ParseArgs(const CompilerInstance& ci, const std::vector<std::string>& args)
	{
		return true;
	}
};

}

static clang::FrontendPluginRegistry::Add<CLPGASTAction> X("CLPGPlugin", "CLang Persistence Generator");


